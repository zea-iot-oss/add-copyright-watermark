# Add Copyright Watermark

Image magick commands to create the watermark file and add the watermark on files

## Install Image magick on MacOS

```
brew install imagemagick
brew install ghostscript
```

## Create the watermark file

```
convert -background transparent -fill 'rgba(255,255,255,0.85)' -font American-Typewriter -pointsize 100 label:'Copyright - Jean-Philippe Blais' -trim +repage cw.png
```

## Add the watermark file

```
composite -gravity SouthEast -geometry +40+20 cw.png DSCF9876.jpeg out/DSCF9876.jpeg
```

## References

- https://www.imagemagick.org/discourse-server/viewtopic.php?t=16234
- https://www.the-art-of-web.com/system/imagemagick-watermark/

